<?php
//ARTURO ZAVALA VAZQUEZ
//Realizar una expresión regular que detecte emails correctos.
$email="militza.aguero@meduca.gob.pa";
$detectaEmail= preg_match('/[A-Za-z0-9._+%-]+@[a-zA-Z]+(\.[a-zA-Z]{2,4})?\.[a-zA-Z]{2,4}/',$email);
echo $detectaEmail;

//Realizar una expresión regular que detecte Curps Correctos
    $curp="ZAVA970210HDFVZR06";
    $detectaCurp= preg_match('/[A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2}/',$curp);
    echo $detectaCurp;

//Realizar una expresión regular que detecte palabras de longitud mayor a 50 formadas solo por letras.
    $cadena="asdfghahshdhdhDDhdjshdjdhdkcmvkcjdhfjdeuckfudjsiufK";
    $detectaCadena= preg_match('/^[a-zA-Z]{51,}/',$cadena);
    echo $detectaCadena;

//Crea una función para escapar los símbolos especiales.
    $simbolos="http://google.com/+hola";
    $escapar= preg_match('/http:\/\/[A-Za-z0-9]+\.\w{2,4}\/\+[a-zA-Z0-9]+/',$simbolos);
    echo $escapar;

//Crear una expresión regular para detectar números decimales.
    $decimales="10.59";
    $detectarDecimales= preg_match('/([0-9]+)?\.[0-9]+/',$decimales);
    echo $detectarDecimales;
?>