<html> 
	<head> 
		<title>Piramide</title> 
	</head> 
	<body> 
        <?php 
/* Para crear la piramide utilizamos un for con un contador y concatenando a la variable piramide nuestro simbolo 
en este caso "*" */
        $piramide = "*";
        $a=1;
        do {
            echo '<center>'.$piramide.'</center>';
            $piramide.="*";
            $a++;
        } while ($a <= 15);
/* Ahora terminamos el rombo, utilice la funcion str_repeat, la cual repite strings por un determinado numero de veces*/
        $a--;
        $rombo = $piramide;
        do {
            --$a;
            $rombo=str_repeat("*",$a);
            echo '<center>'.$rombo.'</center>';
        } while ($a >= 1);
		?>
	</body>
</html> 